<?php
   session_start();
   
   //INCLUDE ALL NECESSARY FILES
   include("dbConnect.php");
   include("doLogin.php");
   include("doSignup.php");
   include("doLogout.php");
   include("inputQuestion.php");
   include("inputRate.php");
   include("verifyEmail.php");
   
   //GETTING ACTION PARAMETER
   $actionPost = 0;
   $actionGet = 0;
   if(isset($_POST["action"])){
      $actionPost = $_POST["action"];
   }
   if(isset($_GET["action"])){
      $actionGet = $_GET["action"];
   }
   
   //CHECKING THE ACTIONS
   if (strcmp($actionPost,"doLogin") == 0){ // DOLOGIN
      doLogin();
   }
   else if(strcmp($actionPost,"doSignup") == 0){ //DOSIGNUP
      doSignup();
   }
   else if(strcmp($actionGet,"doLogout") == 0){ //DOLOGOUT
      doLogout();
   }
   else if(strcmp($actionPost,"inputQuestion") == 0){ //INPUTQUESTION
      inputQuestion();
   }
   else if(strcmp($actionPost,"inputRate") == 0){ //INPUTRATE
      inputRate();
   }
   else if(strcmp($actionGet,"verifyEmail") == 0){ //VERIFYEMAIL
      verifyEmail();
   }
   else{ //IF NO ACTION AT ALL
      header("Location: mainSystem.php");
      //die;
   }
   header("Location: mainSystem.php");
   
?>   