<?php
   //==============================================================================
   //                        showRateOthers
   //==============================================================================
   function showRateOthers(){
      echo "<h2>Step 2 out of 2</h2>";
      echo "<h3>Please rate these questions in order to see your score.</h3>";
      
      $user_id = $_SESSION['user_id'];
      $username = $_SESSION['username'];
      
      $numOfQuestion = NUM_OF_QUESTION;
      $indexCounter = 0;
      $idArr = array();
      $questionArr = array();
      $usedQuestion = array();

      //GET THE QUESTIONS TO BE RATED
      $getQuestion = mysql_query("SELECT q.question_id, q.question FROM question q JOIN user u ON u.user_id = q.user_id WHERE q.user_id !=$user_id AND u.have_rate_others = 1");
      while($resultQuestion = mysql_fetch_array($getQuestion)){
         $idArr[$indexCounter] = $resultQuestion[0];
         $questionArr[$indexCounter] = $resultQuestion[1];
         $indexCounter++;
      }
      $maxIndex = sizeof($idArr) - 1;
     
      echo"<form action='control.php' name='inputRateForm' method='POST' onSubmit='return validateInputRateForm()'>";
      echo"<table>";
      echo"<tr><th>No.</th><th>Question</th><th>Rating</th>";
      echo"</tr>";
      
      //RANDOMIZING THE QS
      for($i = 1; $i <= $numOfQuestion; $i++){
         $randInt = rand(0,$maxIndex);
         
         //RANDOMIZE AGAIN IF ALREADY USED
         while(in_array($randInt, $usedQuestion)){
            $randInt = rand(0,$maxIndex);
         }
         array_push($usedQuestion,$randInt);
         echo "<tr>";
         echo "<td>$i</td>";
         echo "<td>$questionArr[$randInt]</td>";
         echo "<td><select name='rating_$i'>";
         echo "<option value='-'>-</option>";
         echo "<option value='0'>0</option>";
         echo "<option value='1'>1</option>";
         echo "<option value='2'>2</option>";
         echo "<option value='3'>3</option>";
         echo "<option value='4'>4</option>";
         echo "<option value='5'>5</option>";
         echo "<option value='6'>6</option>";
         echo "<option value='7'>7</option>";
         echo "<option value='8'>8</option>";
         echo "<option value='9'>9</option>";
         echo "<option value='10'>10</option>";
         echo "</select></td>";
         echo "</tr>";
         echo "<input type='hidden' name='question_id_$i' value='$idArr[$randInt]'>";
      }
      echo "<tr>";
      echo "<input type='hidden' name='action' value='inputRate'>";
      echo "<td><input type='submit' value='Submit Ratings'></td>";
      echo "</tr>";
      echo "</table>";
      echo "</form>";
      
   }
   
   
?>