<html>
   <head>
      <script type="text/javascript">
         function validateForm(){
            var full_name= document.forms["signupForm"]["full_name"].value;
            var username= document.forms["signupForm"]["username"].value; 
            var email= document.forms["signupForm"]["email"].value;
            var password= document.forms["signupForm"]["password"].value;
            var checkUsername = document.getElementById("DisplayUsernameCheckHere").value;
            var checkEmail = document.getElementById("DisplayEmailCheckHere").value;
            

            if(checkUsername == "Unavailable"){
               alert("Username has been taken. Please choose another username.");
               return false;
            }
            
            else if(checkEmail == "Unavailable"){
               alert("Email has been used. Please choose another email.");
               return false;
            }
            
            else if(full_name==null || full_name=="" || username==null || username==""|| email==null || email==""|| password==null || password==""){
               alert("All fields must be filled out");
               return false;
            }
            
            
         }
         
         function checkUsernameAvailability(string,type){
            var xmlhttp;
            if (string==""){
               document.getElementById("DisplayUsernameCheckHere").innerHTML="";
               return;
            }
            if (window.XMLHttpRequest){
               xmlhttp=new XMLHttpRequest();
            }
            else{
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function(){
               if(xmlhttp.readyState==4 && xmlhttp.status==200){
                  document.getElementById("DisplayUsernameCheckHere").innerHTML = xmlhttp.responseText;
                  document.getElementById("DisplayUsernameCheckHere").value = xmlhttp.responseText;
               }
            }
            xmlhttp.open("GET","checkDataAvailability.php?string="+string+"&type="+type,true);
            xmlhttp.send();
         }
         
         function checkEmailAvailability(string,type){
            var xmlhttp;
            if (string==""){
               document.getElementById("DisplayEmailCheckHere").innerHTML="";
               return;
            }
            if (window.XMLHttpRequest){
               xmlhttp=new XMLHttpRequest();
            }
            else{
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function(){
               if(xmlhttp.readyState==4 && xmlhttp.status==200){
                  document.getElementById("DisplayEmailCheckHere").innerHTML=xmlhttp.responseText;
                  document.getElementById("DisplayEmailCheckHere").value = xmlhttp.responseText;
               }
            }
            xmlhttp.open("GET","checkDataAvailability.php?string="+string+"&type="+type,true);
            xmlhttp.send();
         }
      </script>
   </head>
   
   <body>
      <h1>Sign Up</h1>
      <form action="control.php" name="signupForm" method="POST"  onSubmit="return validateForm()"> 
         
         <table>
            <tr>
               <td>Full Name:</td>
               <td><input type="text" name="full_name"></td>
            </tr>
            <tr>
               <td>Username:</td>
               <td><input type="text" name="username" onKeyUp="checkUsernameAvailability(this.value,'username')"></td>
               <td><div id='DisplayUsernameCheckHere'></div></td>
            </tr>
            <tr>
               <td>Email:</td>
               <td><input type="text" name="email" onKeyUp="checkEmailAvailability(this.value,'email')"></td>
               <td><div id='DisplayEmailCheckHere'></div></td>
            </tr>
            <tr>
               <td>Password:</td>
               <td><input type="password" name="password"></td>
            </tr>
            <tr>
               <input type="hidden" name ="action" value="doSignup">
               <td><input type="submit" value="Sign up"></td>
            </tr>
         </table>
         
      </form>
      
      <br>
      <br>
      <a href="index.php">Home</a>
      
   </body>
</html>
