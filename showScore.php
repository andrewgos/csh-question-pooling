<?php
   //==============================================================================
   //                        showScore
   //==============================================================================
   function showScore(){
      $user_id = $_SESSION['user_id'];
      $username = $_SESSION['username'];
      echo "<h2>All done! Here is your progress.</h2>";
      echo "<table>";
      echo "<tr>";
      echo "<th>No.</th><th>Question</th><th>Ideal Response</th><th>Rating</th>";
      echo "</tr>";

      $numCounter = 1;
      $finalRating = 0;
      
      //GET THE QUESTIONS OF THIS USER
      $getQuestion = mysql_query("SELECT question_id, question, ideal_response FROM question WHERE user_id = $user_id");
      
      //LOOP FOR EACH QUESTION
      while($resultQuestion = mysql_fetch_array($getQuestion)){
         $question_id = $resultQuestion[0];
         $question = $resultQuestion[1];
         $ideal_response = $resultQuestion[2];
         $ratingArr = array();
         $medianRating = 0;
         
         echo "<tr>";
         echo "<td>$numCounter</td>";
         echo "<td>$question</td>";
         echo "<td>$ideal_response</td>";
         
         //PUSHING ALL RATING FOR THIS QUESTION INTO ARRAY
         $getRating = mysql_query("SELECT rating FROM rating WHERE question_id = $question_id");
         while($resultRating = mysql_fetch_array($getRating)){
            array_push($ratingArr,$resultRating);
         }
         //GETTING THE MEDIAN
         rsort($ratingArr);
         $middleIndex = round(count($ratingArr) / 2);
         if($middleIndex != 0){
            $medianRating = $ratingArr[$middleIndex-1];
            $finalRating = $finalRating + $medianRating[0];
            echo "<td>$medianRating[0]</td>";
         }
         else{
            echo "<td>N/A</td>";
         }
         $numCounter++;
         echo "</tr>";
      }
      $finalRating = $finalRating / $numCounter;
      echo "</table>";
      echo "<br>";
      echo "Your Final Rating = ", $finalRating;
   }
   
?>