<?php
   //==============================================================================
   //                        showLeaderboard
   //==============================================================================

   function showLeaderboard(){
      $user_id = $_SESSION['user_id'];
      $username = $_SESSION['username'];
      $userArr = array();
      
      $getUser = mysql_query("SELECT * FROM user");
      
      //LOOP FOR EACH USER & CALCULATE ITS FINAL SCORE
      while($resultUser = mysql_fetch_array($getUser)){
         $curr_user_id = $resultUser[0];
         $curr_username = $resultUser[1];
         $numCounter = 1;
         $finalRating = 0;
         
         //GET THE QUESTIONS OF THIS USER
         $getQuestion = mysql_query("SELECT question_id, question FROM question WHERE user_id = $curr_user_id");
         
         //LOOP FOR EACH QUESTION
         while($resultQuestion = mysql_fetch_array($getQuestion)){
            $question_id = $resultQuestion[0];
            $question = $resultQuestion[1];
            $ratingArr = array();
            $medianRating = 0;
            
            //PUSHING ALL RATING FOR THIS QUESTION INTO ARRAY
            $getRating = mysql_query("SELECT rating FROM rating WHERE question_id = $question_id");
            while($resultRating = mysql_fetch_array($getRating)){
               array_push($ratingArr,$resultRating);
            }
            //GETTING THE MEDIAN
            rsort($ratingArr);
            $middleIndex = round(count($ratingArr) / 2);
            if($middleIndex != 0){
               $medianRating = $ratingArr[$middleIndex-1];
               $finalRating = $finalRating + $medianRating[0];
            }
            else{

            }
            $numCounter++;

         }
         $finalRating = $finalRating / $numCounter;
         
         $userArr[$curr_username] = $finalRating;
      }
      
      echo "<h2>Leaderboard</h2>";
      echo "<table>";
      echo "<tr><th>Rank #</th><th>Name</th><th>Final Score</th>";
      echo "</tr>";
      //$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
      asort($userArr);
      $userArr = array_reverse($userArr, true);
      $rankCounter = 1;
      foreach ($userArr as $key => $val) {
         if($key == $username){
            echo "<tr><td><b>$rankCounter</b></td> <td><b>$key</b></td> <td><b>$val</b></td></tr>";
         }
         else{
            echo "<tr><td>$rankCounter</td> <td>$key</td> <td>$val</td></tr>";
         }
         
         $rankCounter++;
      }
      echo "</table>";
      echo "<br>";
   }
   
?>