-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 10, 2015 at 04:57 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `csh-question-pooling`
--

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `question_id` int(4) NOT NULL AUTO_INCREMENT,
  `user_id` int(4) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `ideal_response` varchar(1000) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`question_id`, `user_id`, `question`, `ideal_response`) VALUES
(1, 4, 'kjnk', 'jnk'),
(2, 4, 'jnm', 'kjn'),
(3, 4, 'kjn', 'kjn'),
(4, 1, 'q1', 'a1'),
(5, 1, 'q2', 'a2'),
(6, 1, 'q3', 'a3'),
(7, 5, 'b1', 'b11'),
(8, 5, 'b2', 'b22'),
(9, 5, 'b3', 'b33'),
(10, 11, 'p1', 'p11'),
(11, 11, 'p2', 'p22'),
(12, 11, 'p3', 'p33'),
(13, 12, 'o1', 'o11'),
(14, 12, 'o2', 'o22'),
(15, 12, 'o3', 'o33'),
(16, 13, 'i1', 'i11'),
(17, 13, 'i2', 'i33'),
(18, 13, 'i2', 'i333232'),
(19, 15, 'poikpoi', 'poipoi'),
(20, 15, 'opiopiop', 'iopiopiop'),
(21, 15, 'oipoioip', 'ioipio'),
(22, 8, 'qq', 'asd'),
(23, 8, 'asd', 'asd'),
(24, 8, 'asdasd', 'asdasd'),
(25, 34, 'What is this?', 'This is a crab ";><;"'),
(26, 34, 'A', 'B'),
(27, 34, 'C', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `user_id` int(4) NOT NULL,
  `question_id` int(4) NOT NULL,
  `rating` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`user_id`, `question_id`, `rating`) VALUES
(4, 5, 6),
(4, 6, 8),
(4, 4, 6),
(7, 5, 5),
(7, 3, 7),
(7, 2, 9),
(1, 2, 10),
(1, 3, 10),
(1, 1, 10),
(5, 3, 3),
(5, 5, 3),
(5, 1, 5),
(11, 6, 10),
(11, 9, 7),
(11, 2, 6),
(12, 9, 4),
(12, 1, 7),
(12, 2, 3),
(13, 4, 5),
(13, 6, 7),
(13, 1, 3),
(15, 17, 8),
(15, 10, 10),
(15, 14, 7),
(16, 17, 8),
(16, 20, 10),
(16, 21, 10),
(8, 21, 10),
(8, 8, 5),
(8, 14, 8),
(34, 4, 10),
(34, 5, 10),
(34, 13, 10);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `have_rate_others` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `full_name`, `email`, `password`, `verified`, `have_rate_others`) VALUES
(1, 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1),
(2, 'test', 'test', 'test', '098f6bcd4621d373cade4e832627b4f6', 0, 0),
(3, '', '', 'asd', 'asd', 0, 0),
(4, 'asd', 'asd', 'asd', '7815696ecbf1c96e6894b779456d330e', 0, 1),
(5, '', '', '', '', 0, 1),
(7, 'asdf', 'asdf', 'asdf', '0cc175b9c0f1b6a831c399e269772661', 0, 1),
(8, 'a', 'a', 'a', '0cc175b9c0f1b6a831c399e269772661', 0, 1),
(9, 'e', 'First Last', 'andrewgosali@gmail.c', 'e1671797c52e15f763380b45e841ec32', 0, 0),
(10, 'k', 'k', 'andrewgosali@gmail.c', '8ce4b16b22b58894aa86c421e8759df3', 0, 0),
(11, 'p', 'p', 'p', '83878c91171338902e0fe0fb97a8c47a', 0, 1),
(12, 'o', 'o', 'o', 'd95679752134a2d9eb61dbd7b91c4bcc', 0, 1),
(13, 'i', 'i', 'i', '865c0c0b4ab0e063e5caa3387c1a8741', 0, 1),
(14, 'u', 'u', 'u', '7b774effe4a349c6dd82ad4f4f21d34c', 0, 0),
(15, 'm', 'mm', 'm', '6f8f57715090da2632453988d9a1501b', 0, 1),
(16, 'n', 'n', 'n', '7b8b965ad4bca0e41ab51de7b31363a1', 0, 1),
(17, 'mm', 'mm', 'andrewgosali@gmail.c', 'b3cd915d758008bd19d0f2428fbb354a', 0, 0),
(18, 'mmm', 'mmm', 'mmm', 'c4efd5020cb49b9d3257ffa0fbccc0ae', 0, 0),
(19, 'mmmm', 'mmmm', 'andrewgosali@gmail.c', '9de37a0627c25684fdd519ca84073e34', 0, 0),
(20, 'mmmmm', 'mmmmm', 'andrewgosali@gmail.c', 'd9308f32f8c6cf370ca5aaaeafc0d49b', 0, 0),
(21, 'mmmmmm', 'mmmmmm', 'andrewgosali@gmail.c', '9aee390f19345028f03bb16c588550e1', 0, 0),
(22, 'pp', 'pp', 'andrew@crowdsourcehi', 'c483f6ce851c9ecd9fb835ff7551737c', 0, 0),
(23, 'ppp', 'ppp', 'andrew@crowdsourcehi', 'f27f6f1c7c5cbf4e3e192e0a47b85300', 0, 0),
(24, 'pppp', 'pppp', 'andrew@crowdsourcehi', '2d7acadf10224ffdabeab505970a8934', 0, 0),
(25, 'ppppp', 'ppppp', 'andrew@crowdsourcehi', 'a7c471cfd3c42dc6d6a8552ac2c0a22c', 0, 0),
(26, 'pppppp', 'pppppp', 'andrew@crowdsourcehi', 'e882b72bccfc2ad578c27b0d9b472a14', 0, 0),
(27, 'ppppppp', 'ppppppp', 'andrew@crowdsourcehi', '7030ee1b8aa5481643b9ac899638f8d7', 0, 0),
(28, 'pppppppp', 'pppppppp', 'andrew@crowdsourcehi', 'b4fb8c802583d75c36858811115b6272', 0, 0),
(29, 'pppppppp', 'ppppppppp', 'andrew@crowdsourcehi', 'b4fb8c802583d75c36858811115b6272', 0, 0),
(30, 'pppppppppp', 'pppppppppp', 'andrew@crowdsourcehi', '995bf3394c81c556054aad920e0f2513', 0, 0),
(31, 'pppppppppp', 'ppppppppppp', 'andrew@crowdsourcehi', 'd53b710b61c13b45675caf925d43a0c2', 0, 0),
(32, 'pppppppppp', 'pppppppppppp', 'andrew@crowdsourcehi', 'ccebefdad0ceaf2e9ad9aeafdaf66696', 0, 0),
(33, 'pppppppppp', 'ppppppppppppp', 'andrew@crowdsourcehi', '75beb1c4d7f97a8462d8878e721b2357', 0, 0),
(34, 'T', 'T', 'T', 'e358efa489f58062f10dd7316b65649e', 0, 1),
(35, 'jk', 'jk', 'andrew@crowdsourcehi', '051a9911de7b5bbc610b76f4eda834a0', 1, 0),
(36, 'qqq', 'qqq', 'andrewgosali@gmail.com', 'b2ca678b4c936f905fb82f2733f5297f', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
