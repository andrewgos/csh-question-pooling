<html>
   <head>
      <script type="text/javascript">
         function validateForm(){
            var username= document.forms["loginForm"]["username"].value; 
            var password= document.forms["loginForm"]["password"].value;
            
            if(username==null || username==""|| password==null || password==""){
               alert("All fields must be filled out");
               return false;
            }
         }
      </script>
   </head>
   
   <body>
      <h1>Log In</h1>
      <form action="control.php" name="loginForm" method="POST" onSubmit="return validateForm()"> 
         
         <table>
            <tr>
               <td>Username:</td>
               <td><input type="text" name="username"></td>
            </tr>
            <tr>
               <td>Password:</td>
               <td><input type="password" name="password"></td>
            </tr>
            <tr>
               <input type="hidden" name ="action" value="doLogin">
               <td><input type="submit" value="Login"></td>
            </tr>
         </table>
         
      </form>
      
      <br>
      <br>
      <a href="index.php">Home</a>
      
   </body>
</html>
