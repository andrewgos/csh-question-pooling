<?php
   //==============================================================================
   //                        showInputQuestion
   //==============================================================================
   function showInputQuestion(){
      echo "<h2>Step 1 out of 2</h2>";
      echo "<h3>Please input your questions in order to see your score.</h3>";
      $numOfQuestion = NUM_OF_QUESTION;
      echo"<form action='control.php' name='inputQuestionForm' method='POST' onSubmit='return validateInputQuestionForm()'>";
      echo"<table>";
      for($i = 1; $i <= $numOfQuestion; $i++){
         echo "<tr>";
         echo "<td>Question $i:<br><textarea name='question_$i' style='height: 15em;width: 35em;'></textarea></td>";
         echo "<td>Ideal Response $i:<br><textarea name='ideal_response_$i' style='height: 15em;width:35em;'></textarea></td>";
         echo "</tr>";
      }
      echo "<tr>";
      echo "<input type='hidden' name='action' value='inputQuestion'>";
      echo "<td><input type='submit' value='Submit Questions'></td>";
      echo "</tr>";
      echo "</table>";
      echo "</form>";
   }
   
   
?>