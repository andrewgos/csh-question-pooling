<?php
   define("NUM_OF_QUESTION", 3);
?>
<html>
   <head>
   
      <script type="text/javascript">
      function validateInputQuestionForm(){
         var numOfQuestion = <?php echo NUM_OF_QUESTION;?>;
         
         for (var i = 1; i <= numOfQuestion; i++) {
            var question = "question_";
            var ideal_response = "ideal_response_";
            
            question = question.concat(i.toString());
            ideal_response = ideal_response.concat(i.toString());
   
            var question= document.forms["inputQuestionForm"][question].value;
            var ideal_response= document.forms["inputQuestionForm"][ideal_response].value;
            
            if(question==null || question=="" || ideal_response==null || ideal_response==""){
               alert("All fields must be filled out");
               return false;
            }
         }
      }
      function validateInputRateForm(){
         var numOfQuestion = <?php echo NUM_OF_QUESTION;?>;
         for (var i = 1; i <= numOfQuestion; i++) {
            var rating = "rating_";
            rating = rating.concat(i.toString());
            var rating= document.forms["inputRateForm"][rating].value;
            
            if(rating=="-"){
               alert("Please rate all the questions first.");
               return false;
            }
         }
      }
      </script>
   </head>
   
   <body>
      
      
      
<?php
   session_start();
   include("dbConnect.php");
   include("showInputQuestion.php");
   include("showRateOthers.php");
   include("showScore.php");
   include("showLeaderboard.php");
   

   if(isset($_SESSION["username"])){ //IF LOGGED IN
      $user_id = $_SESSION['user_id'];
      $username = $_SESSION['username'];
      echo "You are logged in as: ", $_SESSION["username"];
      echo "<br>";
      echo "<a href='control.php?action=doLogout'>Log Out</a>";
      echo "<br>";
      echo "<br>";
      
      //CHECKING WHAT THIS USER HAS DONE
      $haveVerifyEmail = checkHaveVerifyEmail($user_id); //CHECK IF THEY HAVE VERIFY THEIR EMAIL
      $haveInputQuestion = checkHaveInputQuestion($user_id); //CHECK IF THEY HAVE INPUT QUESTIONS
      $haveRateOthers = checkHaveRateOthers($user_id); //CHECK IF THEY HAVE RATE OTHERS
      
      if(!$haveVerifyEmail){ //IF THEY HAVENT VERIFY THEIR EMAIL
         echo "<h3>Please verify your email before proceeding.</h3>";
         echo "<br>";
      }
      
      else if(!$haveInputQuestion){ //IF THEY HAVENT INPUT THE QUESTION
         showInputQuestion();
      }
      
      else if(!$haveRateOthers){ //IF THEY HAVENT RATE OTHERS
         showRateOthers();
      }
      
      else if($haveInputQuestion && $haveRateOthers){ //IF THEY HAVE DONE BOTH
         showScore();
         echo "<hr/>";         
         showLeaderboard();
      }
   }
   else{ //IF NOT LOGGED IN
      echo "Please Login";
      echo "<br>";
      echo "<br>";
      echo "<a href='login.php'>Log In</a>  ";
      echo "or <a href='signup.php'>Sign Up</a>";
      echo "<br>";
      echo "<br>";
      echo "<a href='index.php'>Home</a>";
   }
   
//==============================================================================
//                           HELPER FUNCTIONS
//==============================================================================
 
 
   //==============================================================================
   //                        checkHaveRateOthers
   //==============================================================================
   function checkHaveRateOthers($user_id){
      $checkHaveRateOthers = mysql_query("SELECT have_rate_others FROM user WHERE user_id='$user_id'");
      while($resultHaveRateOthers = mysql_fetch_array($checkHaveRateOthers)){
         if($resultHaveRateOthers[0] == 1){
            return 1;
         }
      }
      return 0;
   }
   //==============================================================================
   //                        checkHaveInputQuestion
   //==============================================================================
   function checkHaveInputQuestion($user_id){
      $checkHaveInputQuestion = mysql_query("SELECT * FROM question WHERE user_id='$user_id'");
      while($resultHaveInputQuestion = mysql_fetch_array($checkHaveInputQuestion)){
         return 1;
      }
      return 0;
   }
   
   //==============================================================================
   //                        checkHaveVerifyEmail
   //==============================================================================
   function checkHaveVerifyEmail($user_id){
      $checkHaveVerifyEmail = mysql_query("SELECT verified FROM user WHERE user_id='$user_id'");
      while($resultHaveVerifyEmail = mysql_fetch_array($checkHaveVerifyEmail)){
         return $resultHaveVerifyEmail[0];
      }
      return 0;
   }
      
   
   
?>

   
   </body>
</html>
